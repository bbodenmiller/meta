# GitLab Community Forks

## Why

- Promote collaboration (with team members and other wider community members).
- Simplify the community contribution process/experience.
- Avoid issues with quotas (CI minutes, storage etc).
- Remove the need to continually rebase forks from upstream.
- Support taking over/finishing wider community merge requests.

## How

### Request access to community forks

Simply [request access](https://docs.gitlab.com/ee/user/group/manage.html#request-access-to-a-group)
to the [`gitlab-community/community-members` group](https://gitlab.com/gitlab-community/community-members).

We use a subgroup to allow additional administrators to manage access without becoming owners of the main group.

### Working in a community fork

- Clone the fork.
- Create a new branch.
- Push the branch.
- Create merge request.

#### GitLab project specifics

- For **existing GitLab Development Kit (GDK) installations**:
  - `rm -rf gitlab` from inside your `gitlab-development-kit` folder.
  - Then `git clone https://gitlab.com/gitlab-community/gitlab.git`.

  > **Note**: Anything that lives outside the `gitlab` folder, for example, the database, won't get affected by this.

- For **new GitLab Development Kit (GDK) installations**:
  - Enter `https://gitlab.com/gitlab-community/gitlab.git` when prompted for the GitLab repo URL.

### Request a new community fork

Create an issue in the [issue tracker](https://gitlab.com/gitlab-community/meta/-/issues)
and tag @gitlab-org/quality/contributor-success.

### Team member process for creating a new community fork

#### Fork the project

The `gitlab-community` group structure should mimic the `gitlab-org` group structure.
Create subgroups where required. For example, the [gemnasium project](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium)
is forked from `gitlab-org/security-products/analyzers` to [`gitlab-community/security-products/analyzers`](https://gitlab.com/gitlab-community/security-products/analyzers/gemnasium).

When creating a new subgroup, change the following settings in **Permissions and group features**:

- Uncheck **Users can request access**.
- Set the Group Wiki to **Disabled**.
- Set **Roles allowed to create projects** to **Maintainers**

1. Select `gitlab-community`, or the relevant subgroup as the target namespace.
1. Use the same name suffixed with `Community Fork`.
1. Use the same slug.
1. Use the description: `For information about community forks, checkout the [meta project](https://gitlab.com/gitlab-community/meta)`.

#### Update the project settings

From **Settings > General > Visibility, project features, permissions** uncheck:

- **Users can request access**.
- **Issues**.
- **CVE requests**.
- **Forks**.
- [**Container registry**](#container-registry).
- **Requirements**.
- **Security & Compliance**.
- **Wiki**.
- **Snippets**.
- [**Package registry**](#package-registry).
- **Pages**.
- **Monitor**.
- **Environments**.
- **Feature flags**.
- **Infrastructure**.
- **Releases**.

#### Container Registry

If you know the project uses the Container Registry in its pipeline, leave **Container registry** enabled,
and configure a Cleanup Policy at **Settings > Packages and registries > Edit cleanup rules** with the smallest values:

- Keep the most recent: **1 tag per image name**.
- Remove tags older than: **7 days**.

If you are unsure, disable it.

Failures like `invalid argument "some-image:some-tag" for "-t, --tag" flag: invalid reference format`
or `error checking push permissions -- make sure you entered the correct tag name, and that you are authenticated correctly, and try again: checking push permission for`
indicate that the Container Registry is required by the pipeline.

#### Package Registry

If you know the project uses the Package Registry in its pipeline, leave **Package registry** enabled.

If you are unsure, disable it.

Failures like `ERROR: Errors uploading some packages` mostly indicate that the Package Registry is required by the pipeline.

This error message will look different accross all package managers or build tools, but they will normally contain some
error which occurred while trying the deployment of a package.

#### Configure pull mirroring

1. [Create a project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token).
   1. Use the name `community fork mirroring`.
   1. Grant **Maintainer** permission.
   1. Enable **API** and **Write Repository** scopes.
1. From **Settings > Repository > Protected branches** set it so:
   - **No one** can merge.
   - **No one** except the **community fork mirroring project bot** can push to `master`/`main`.
1. Delete all branches except `master`/`main`.
1. Enabling pull mirroring using the project access token:
   ```
   curl --request PUT \
        --header "PRIVATE-TOKEN: <token>" \
        --data "import_url=https://gitlab.com/gitlab-org/<canonical>.git&mirror=true&mirror_trigger_builds=false&only_mirror_protected_branches=true&shared_runners_enabled=true&mirror_overwrites_diverged_branches=true" \
        --url 'https://gitlab.com/api/v4/projects/<community-fork-project-id>'
   ```
   **NOTE**: We setup pull mirroring using a project access token to avoid the mirroring activity flooding our personal activity feeds/profiles.

### Migrate an existing merge request into the community fork

Eventually, we hope all merge requests will come from the community fork, and this process will become redundant.
Until then, please follow the [takeover of the community merge request](https://docs.gitlab.com/ee/development/code_review.html#taking-over-a-community-merge-request) process,
recreating the branch in the respective [community fork repository](https://gitlab.com/gitlab-community).

You do not need to add the canonical project or source fork, you can pull directly:

```shell
git fetch https://gitlab.com/fork-namespace/gitlab.git their-branch:our-name-for-branch
```

## Volunteering to support the community forks

By using the community forks, you are already being a great help, thank you!

There are no predefined criteria for becoming a maintainer.
Please reach out on [Discord](https://discord.gg/gitlab) if you would like to help out.

## Troubleshooting

### Failing Pipelines

See [container registry](#container-registry) and [package registry](#package-registry) for potential reasons for pipeline failure.
